# Crawls pushshift and adds unique links to discord_crawler_discord_server database
#
# v0.5.1 testing 7/29/19
# - caching ambigious server IDs on-crawl 
# -------------------------------------------------------------------------------------\

import psycopg2, psycopg2.extras
import urllib.parse
import sys, json, re, requests, sys, time, os
from free_proxy import FreeProxy
if __debug__:
  import traceback
  import logging

def log(msg=None, isError=False, e=None):
  if not isError and msg:
    print('*** [dcrawl-svc] ' + msg                 + ' \n')
  else:
    print ('*** [ERROR] ' + str(sys.exc_info()[1])  + ' \n')
    if __debug__: logging.error(traceback.format_exc())

# -------------------------------------------------------------------------------------\

class CrawlService():
  conn                = None
  dict_cur            = None
  cached_guild_ids    = list()
  cached_invite_links = list()
  cached_invalid      = list()
  proxy               = None
  ps_api              = 'https://api.pushshift.io/reddit/search/comment/?q=discord.gg&size=999&field=body'
  discord_api         = 'https://discordapp.com/api/v6/invites/' #invitecode?&with_counts=true
  headers             = { "Accept": "*/*", "Accept-Encoding": "gzip, deflate, br", "Accept-Language": "en-US",  \
                          "Referer": "http://reddit.com/", "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64)   \
                          AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
                          "X-HTTP-Method": "GET" }

  # ------------------------------------------------------------------------------------------------------------\
  def connectDB(self):
    try:
      self.conn       = psycopg2.connect(database='dc', user='dcrawler', password='dcrawler')
      self.dict_cur   = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
      if __debug__: log('Connected to database')
      return True

    except Exception as e: 
      log(None, True, e)
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def cycleProxy(self):
    keepChecking = True
    while (keepChecking is True):
      try:
        proxy         = FreeProxy(rand=True).get()
        proxyDict     = {"https" : proxy }  
        if __debug__: log('Using proxy ' + str(proxyDict))
        self.proxy    = proxyDict
        keepChecking  = False
      except Exception as e:
        if __debug__: log('Trying another proxy: ' + str(e))
        keepChecking  = True

  # ------------------------------------------------------------------------------------------------------------\
  def generateEndPoint(self, from_code):
    return(self.discord_api + from_code + '?with_counts=true')

  # ------------------------------------------------------------------------------------------------------------\
  def getPushshiftComments(self):
    try:
      a_list          = list()
      contents        = urllib.request.urlopen(self.ps_api).read()
      json_data       = json.loads(contents)['data']
      for comment in json_data:
        another_list  = self.parseInvitesFromComment(comment['body'])
        the_subreddit = comment['subreddit']
        the_parentid  = comment['parent_id']
        if another_list:
          for cmt in another_list:
            a_list.append( {  'subreddit': the_subreddit, 
                              'invite_link': cmt,
                              'parent_id': the_parentid  } )          
      return a_list
    except Exception as e:
      pass

  # ------------------------------------------------------------------------------------------------------------\
  def checkIDExists(self, id):
    if self.cached_guild_ids is not None:
      if id in self.cached_guild_ids:
        #log('id ' + id + ' is already cached..')
        return True
      else:
        return False
    else:
      if __debug__: log("Warning: can't check validity before caching tables")
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def checkLinkValid(self, link):
    if self.cached_invite_links is not None:
      if link in self.cached_invite_links:
        #log('link ' + link + ' is already cached..')
        return True
      else:
        return False
    else:
      if __debug__: log("Warning: can't check validity before caching tables")
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def checkLinkInvalid(self, link):
    if self.cached_invalid is not None:
      if link in self.cached_invalid:
        return True
      else:
        return False
    else:
      if __debug__: log("Warning: can't check validity before caching tables")
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def checkNSFW(self, parent_id):
    detail_api  = 'https://api.pushshift.io/reddit/search/submission/?ids=' + parent_id
    try:
      contents    = urllib.request.urlopen(detail_api).read()
      json_data   = json.loads(contents)['data']
      over_18     = str(json_data[0]['over_18'])
      log(over_18)
      if over_18 is 'True':
        return 1
      elif over_18 is 'False':
        return 0
      else:
        return 2
    except:
      return 2

  # ------------------------------------------------------------------------------------------------------------\
  def cacheAll(self):
    try:
      #if __debug__: log("Caching all validated servers...")
      self.dict_cur.execute("select * from discord_crawler_discord_server")
      all_records = self.dict_cur.fetchall() 
      for record in all_records:
        self.cached_invite_links.append(record[8])  # 8th column = invite_link
        #if __debug__: log('Caching guild ID: ' + str(record[5]))
        #if __debug__: log('Caching invite link: ' + str(record[8]))
        self.cached_guild_ids.append(record[5])     # 5th column = guild_id
      #if __debug__: log("Caching all invalid servers...")
      self.dict_cur.execute("select * from discord_crawler_invalid_link")
      all_records = self.dict_cur.fetchall() 
      for record in all_records:
        self.cached_invalid.append(record[1])       # 1st column = invite_link
      return True
    except Exception as e:
      log(None, True, e)
      return False

    # ------------------------------------------------------------------------------------------------------------\
  def addCrawledServer    ( self, approximate_member_count, approximate_presence_count, 
                           invite_link, guild_icon, guild_id, guild_name, guild_splash, 
                           guild_description, subreddit, nsfw ):
    try:
      now                 =   str ( int( round ( time.time ( ) ) ) )
      data                = [ approximate_member_count, approximate_presence_count, 
                              invite_link.rstrip(), guild_icon, guild_id,
                              guild_name, guild_splash, guild_description, 
                              now, now, subreddit, nsfw ]    
      self.dict_cur.execute ( "INSERT into discord_crawler_discord_server(approximate_member_count, \
                               approximate_presence_count, invite_link, guild_icon, guild_id,       \
                               guild_name, guild_splash, guild_description, time_added,             \
                               time_last_count, related_subredit, nsfw)                             \
                               VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", data)
      self.conn.commit()
      self.cached_invite_links.append(invite_link)
      self.cached_guild_ids.append(guild_id)
      if __debug__: log('Added server ' + invite_link + ' to database')
      return True
    except Exception as e:
      log(None, True, e)
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def addInvalidServer( self, invite_link ):
    try:
      self.dict_cur.execute("INSERT INTO discord_crawler_invalid_link(invite_link) VALUES (%s)", [invite_link])
      self.conn.commit()
      self.cached_invalid.append(invite_link)
      return True
    except Exception as e:
      log(None, True, e)
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def closeDB(self):
    try:
      self.dict_cur.close()
      self.conn.close()
      if __debug__: log('Closed database connection')
    except: pass

  # ------------------------------------------------------------------------------------------------------------\
  def parseInvitesFromComment(self, comment):
    regExp      =   r'\(([^()]*)\)'
    matches     =   list()
    found       =   list()
    matches     =   re.findall(regExp, comment)  
    ct          =   0
    for link    in  matches:
      try       :
        invite  =   link.split('.gg/')[1] 
        ct      +=  1
        found.append(invite) 
      except Exception as e: pass
    if len(found) > 0:  
      return found
    else: 
      words       = None
      code        = None
      is_words    = comment.find(' '            )
      if is_words > -1:
        words     = comment.split(' '           )
        for word in words:
          is_disclink     = word.find('.gg/'    )
          if is_disclink  > -1:
            code          = word.split('.gg/'   )
            if code: code = code[1]
      else:
        is_disclink       = comment.find('.gg/' )
        if is_disclink    > -1:
          code            = comment.split('.gg/') [1]
      if code:
        found.append        (code)
        return              [code]
      else:
        return              None

  # ------------------------------------------------------------------------------------------------------------\
  def crawl(self):
    ps_comments       =  self.getPushshiftComments ()
    to_crawl          =  list()
    if                   ps_comments               :
      for link        in ps_comments               :
        skipThisLink   = False
        invite         = link ['invite_link']
        subreddit      = link ['subreddit']
        parent         = link ['parent_id']
        if self.checkLinkValid  (invite) is True   : skipThisLink = True
        if self.checkLinkInvalid(invite) is True   : skipThisLink = True
        if skipThisLink                  is False  :
          if  (len(invite)>3) & (len(invite)<8):
            strpos = invite.find (' ')
            if (strpos>0):        next
            strpos = invite.find ('#')
            if (strpos>0):        next
            strpos = invite.find ('"')
            if (strpos>0):        next
            strpos = invite.find ('-')
            if (strpos>0):        next
            strpos = invite.find (':')
            if (strpos>0):        next                       
            to_crawl.append( { 'subreddit'   : subreddit, 
                               'invite_link' : str(invite),
                               'parent_id'   : parent   } )

      log('Found ' + str(len(to_crawl)) + ' new links to crawl\n' )
      if len(to_crawl) > 0:
        self.resolveInviteLinks(to_crawl)

  # ------------------------------------------------------------------------------------------------------------\
  def resolveInviteLinks(self, links):
    self.cycleProxy  ()
    crawled           = 0
    for link in links : 
      if __debug__    : log(' Subreddit: ' + link['subreddit'      ] 
                      +     ' Link: '      + link['invite_link'    ] + ' Parent: ' + link['parent_id'])
      invite          =                      link['invite_link'    ]
      subreddit       =                      link['subreddit'      ]
      parent          =                      link['parent_id']
      noBueno         = False
      if  self.checkLinkValid  (invite)     is not True and        \
          self.checkLinkInvalid(invite)     is not True            :
        endpoint   = self.generateEndPoint  (invite)
        if __debug__: log('Hitting '        + endpoint)
        try:
          response = requests.get(endpoint, headers = self.headers  ,
                                            proxies = self.proxy    )     
        except Exception as e:
          log('Failed, cycling: ' + str(e))
          noBueno  = True
        if not noBueno:
          if __debug__: log(str(response.text))
          errCheck          = True
          dictResponse      = dict()
          while (errCheck is True):
            try:
              dictResponse  = json.loads(str(response.text))
              errCheck      = False
            except Exception as e :
              errCheck      = True
              if __debug__: log('Suspected ratelimit! Cycling proxy..')
              self.cycleProxy()
          if (response.status_code==200): # good server
            if self.checkIDExists(dictResponse['guild']['id']):
              log('Caching ambigious server ID..')
              self.cached_invite_links.append(dictResponse['code'])  
            else:
              nsfwSatus = self.checkNSFW(parent)

              self.addCrawledServer(  dictResponse['approximate_member_count'], dictResponse['approximate_presence_count'], dictResponse['code'], dictResponse['guild']['icon'], dictResponse['guild']['id'], dictResponse['guild']['name'], dictResponse['guild']['splash'], dictResponse['guild']['description'], subreddit, nsfwSatus)
              crawled += 1
              if __debug__: log('Sucessfully crawled ' + dictResponse['guild']['name'] + ' [' + invite + '] with NSFW status ' + str(nsfwSatus))
          elif (response.status_code == 429):
            if __debug__: log('RATE LIMIT HIT! Cycling proxy..')
            self.cycleProxy()
          elif (response.status_code == 404):
            self.addInvalidServer(invite)
            if __debug__: log('Invalid server. Adding to INVALID table...\n')
          else:
              log('FATAL: Response unknown: ' + str(response.status_code))
              exit()
        else:
          self.cycleProxy()
                    
# ------------------------------------------------------------------------------------------------------------\

# vars
infiniteCrawl = True

# init
test = CrawlService()
didConnect = test.connectDB()
if didConnect:
  test.cacheAll()

  if infiniteCrawl:
    log('Infinite crawl is TRUE.  Kill with SysInterrupt!')
    while(True):
      test.crawl()
      log('Sleeping 10 seconds')
      time.sleep(10)
  else: # run tests here
    log('Getting comments..')
    comments = test.getPushshiftComments()
    for comment in comments:
      log(str(comment))
      log('This comment parent: ' + comment['parent_id'])
      log('checkNSFW: ' + str(test.checkNSFW(comment['parent_id'])))

else:
  log("Fatal error: couldn't connect to database. ")

# on closing
test.closeDB()
