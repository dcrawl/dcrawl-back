from discord_crawler.api.viewsets import *
from rest_framework import routers

router = routers.DefaultRouter()
router.register('Discord_Server', Discord_ServerViewSet, base_name='Discord_Server')
