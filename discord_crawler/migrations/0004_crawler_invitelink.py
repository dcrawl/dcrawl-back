# Generated by Django 2.1.7 on 2019-07-25 01:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('discord_crawler', '0003_auto_20190724_2042'),
    ]

    operations = [
        migrations.CreateModel(
            name='crawler_invitelink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('discord_link', models.CharField(blank=True, max_length=64, null=True)),
            ],
        ),
    ]
