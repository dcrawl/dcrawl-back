# Generated by Django 2.1.7 on 2019-07-22 22:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discord_crawler', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='new_link',
            new_name='New_Feed',
        ),
    ]
