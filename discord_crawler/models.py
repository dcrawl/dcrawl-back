from django.db import models
from django.contrib.postgres.fields import ArrayField

class invalid_link(models.Model):
  invite_link  = models.CharField(max_length = 16, null = True, blank = True)

class Discord_Server(models.Model):
  approximate_member_count      = models.IntegerField(default=-1                               )
  approximate_presence_count    = models.IntegerField(default=-1                               )
  invite_link                   = models.CharField(null = True, blank = True, max_length = 16  )
  guild_icon                    = models.CharField(null = True, blank = True, max_length = 128 )
  guild_id                      = models.CharField(null = True, blank = True, max_length = 32  )
  guild_name                    = models.CharField(null = True, blank = True, max_length = 128 )
  guild_splash                  = models.CharField(null = True, blank = True, max_length = 128 )
  guild_description             = models.CharField(null = True, blank = True, max_length = 256 )
  time_added                    = models.BigIntegerField(default=0                             )
  time_last_count               = models.BigIntegerField(default=0                             )
  related_subredit              = models.CharField(null = True, blank = True, max_length = 64  )
  tags                          = ArrayField(models.CharField(null   = True, blank = True, 
                                             max_length = 80 ), size = 20,   null  = True      ) 

  # 0=false, 1=true, 2=unknown
  nsfw                          = models.IntegerField(default=2                                ) 

  class Meta:
    ordering = ['-approximate_presence_count'] # default-order by time_added [descending order]