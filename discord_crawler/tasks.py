from __future__ import absolute_import, unicode_literals
from celery import task
from .crawlservice import CrawlService
from .countservice import CountView
from django.core.cache import cache
from .models import Discord_Server


@task(name="dcount1")
def dcount1():
  stop = str(cache.get('EMERGENCY_STOP', 'Disabled'))
  if 'Disabled' in stop:
    status = str(cache.get('COUNT1_STATUS', 'None'))
    print('*** [dcount] COUNT1_STATUS from cache: ' + str(status))
    if status is 'None':
      cache.set('COUNT1_STATUS', 'Idle')
      status = 'Idle'
    if 'Counting' in status:
      print('*** [dcount] already counting - skipping crawl...')
    elif 'Idle' in status:
      print('*** [dcount] launching COUNT UPDATE task on chunk 1')
      count = Discord_Server.objects.count()
      firstchunk = (1* (count / 5))
      print('*** start_at: 0, amount: ' + str(firstchunk))
      test = CountView()
      cache.set('COUNT1_STATUS', 'Counting')
      print('*** [dcrawlps] Counting [1]..\n')
      test.updateCounts(0, firstchunk)
      cache.set('COUNT1_STATUS', 'Idle')

@task(name="dcount2")
def dcount1():
  stop = str(cache.get('EMERGENCY_STOP', 'Disabled'))
  if 'Disabled' in stop:
    status = str(cache.get('COUNT2_STATUS', 'None'))
    print('*** [dcount] COUNT2_STATUS from cache: ' + str(status))
    if status is 'None':
      cache.set('COUNT2_STATUS', 'Idle')
      status = 'Idle'
    if 'Counting' in status:
      print('*** [dcount] already counting - skipping crawl...')
    elif 'Idle' in status:
      print('*** [dcount] launching COUNT UPDATE task on chunk 2')
      count = Discord_Server.objects.count()
      chunksize = count / 5
      start = chunksize * 2
      print('*** start_at: ' + str(start) + ',  amount: ' + str(chunksize))
      test = CountView()
      cache.set('COUNT2_STATUS', 'Counting')
      print('*** [dcount] Counting [2]..\n')
      test.updateCounts(start, chunksize)
      cache.set('COUNT2_STATUS', 'Idle')


@task(name="dcount3")
def dcount3():
  stop = str(cache.get('EMERGENCY_STOP', 'Disabled'))
  if 'Disabled' in stop:
    status = str(cache.get('COUNT3_STATUS', 'None'))
    print('*** [dcount] COUNT3_STATUS from cache: ' + str(status))
    if status is 'None':
      cache.set('COUNT3_STATUS', 'Idle')
      status = 'Idle'
    if 'Counting' in status:
      print('*** [dcount] already counting - skipping crawl...')
    elif 'Idle' in status:
      print('*** [dcount] launching COUNT UPDATE task on chunk 2')
      count = Discord_Server.objects.count()
      chunksize = count / 5
      start = chunksize * 3
      print('*** start_at: ' + str(start) + ',  amount: ' + str(chunksize))
      test = CountView()
      cache.set('COUNT3_STATUS', 'Counting')
      print('*** [dcount] Counting [3]..\n')
      test.updateCounts(start, chunksize)
      cache.set('COUNT3_STATUS', 'Idle')


@task(name="dcount4")
def dcount4():
  stop = str(cache.get('EMERGENCY_STOP', 'Disabled'))
  if 'Disabled' in stop:
    status = str(cache.get('COUNT4_STATUS', 'None'))
    print('*** [dcount] COUNT4_STATUS from cache: ' + str(status))
    if status is 'None':
      cache.set('COUNT4_STATUS', 'Idle')
      status = 'Idle'
    if 'Counting' in status:
      print('*** [dcount] already counting - skipping crawl...')
    elif 'Idle' in status:
      print('*** [dcount] launching COUNT UPDATE task on chunk 4')
      count = Discord_Server.objects.count()
      chunksize = count / 5
      start = chunksize * 4
      print('*** start_at: ' + str(start) + ',  amount: ' + str(chunksize))
      test = CountView()
      cache.set('COUNT2_STATUS', 'Counting')
      print('*** [dcount] Counting [4]..\n')
      test.updateCounts(start, chunksize)
      cache.set('COUNT4_STATUS', 'Idle')

@task(name="dcount5")
def dcount5():
  stop = str(cache.get('EMERGENCY_STOP', 'Disabled'))
  if 'Disabled' in stop:
    status = str(cache.get('COUNT5_STATUS', 'None'))
    print('*** [dcount] COUNT5_STATUS from cache: ' + str(status))
    if status is 'None':
      cache.set('COUNT5_STATUS', 'Idle')
      status = 'Idle'
    if 'Counting' in status:
      print('*** [dcount] already counting - skipping crawl...')
    elif 'Idle' in status:
      print('*** [dcount] launching COUNT UPDATE task on chunk 5')
      count = Discord_Server.objects.count()
      chunksize = count / 5
      start = chunksize * 5
      print('*** start_at: ' + str(start) + ',  amount: ' + str(chunksize))
      test = CountView()
      cache.set('COUNT2_STATUS', 'Counting')
      print('*** [dcount] Counting [5]..\n')
      test.updateCounts(start, chunksize)
      cache.set('COUNT5_STATUS', 'Idle')

@task(name="dcrawlps")
def dcrawlps():
  status = str(cache.get('CRAWL_STATUS', 'None'))
  print('Crawl status from cache: ' + str(status))
  if status is 'None':
    print('*** [dcrawlps] setting initial CRAWL_STATUS')
    cache.set('CRAWL_STATUS', 'Idle')
    status = 'Idle'

  if 'Crawling' in status:
    print('*** [dcrawlps] already crawling - skipping crawl...')
  elif 'Idle' in status:  
    print('*** [dcrawlps] launched, launching crawl worker..\n')
    test = CrawlService()
    print('*** [dcrawlps] Crawling..\n')
    cache.set('CRAWL_STATUS', 'Crawling')
    test.crawl()
    cache.set('CRAWL_STATUS', 'Idle')
  else:
    print('*** WARNING: UNKNOWN CRAWL STATE. RESTART DYNOS\n')

  return CRAWL_SUCCESS
