from django.apps import AppConfig


class DiscordCrawlerConfig(AppConfig):
    name = 'discord_crawler'
