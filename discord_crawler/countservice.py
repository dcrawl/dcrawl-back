
from django.http import HttpResponse
from django.conf import settings
from django.views import View
from .models import Discord_Server 
from django.template.context_processors import csrf
import sys, json, re, requests, sys, time, os
from free_proxy import FreeProxy
from django.core.cache import cache


def log(str):
  print('*** [dcount-svc] ' + str + '\n')

class CountView(View):
  model   = Discord_Server
  action  = None
  proxy   = None
  discord_api         = 'https://discordapp.com/api/v6/invites/' #invitecode?&with_counts=true
  headers             = { "Accept": "*/*", "Accept-Encoding": "gzip, deflate, br", "Accept-Language": "en-US",  \
                          "Referer": "http://reddit.com/", "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64)   \
                          AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
                          "X-HTTP-Method": "GET" }

  def cycleProxy(self):
    keepChecking = True
    iters = 0
    while keepChecking is True:
      try:
        iters += 1
        if iters > 20:
          keepChecking = False
          log('Failed 20 times, quitting crawl..')
          cache.set('CRAWL_STATUS', 'Idle')
        proxy = FreeProxy(rand=True).get()
        proxyDict = {"https" : proxy }  
        if __debug__: log('Using proxy ' + str(proxyDict))
        self.proxy = proxyDict
        keepChecking = False
      except:
        log('Trying another proxy.. (cycle ' + str(iters) + ')')
        keepChecking = True

  # ------------------------------------------------------------------------------------------------------------\
  def generateEndPoint(self, from_code):
    return(self.discord_api + from_code + '?with_counts=true')

  # ------------------------------------------------------------------------------------------------------------\
  def updateCounts(self, start_at, amount):
    self.cycleProxy()
    updated = 0
    list_all = False
    queryset = Discord_Server.objects.filter(id__gte=start_at)
    queryset = queryset.filter(id__lte=(start_at+amount))

    for server in queryset : 
      stop = cache.get('EMERGENCY_STOP')
      if not 'Enabled' in stop:
        invite = server.invite_link
        oldcounts = server.tags
        log('old counts: ' + str(oldcounts) + '\n')
        noBueno = False
      
        endpoint   = self.generateEndPoint(invite)
        if __debug__: log('yeeting digits from ' + endpoint)
        try:
          response = requests.get(endpoint, headers = self.headers  ,
                                            proxies = self.proxy    )     
        except Exception as e:
          log('caught exception: ' + str(e))
          noBueno  = True
        if not noBueno:
          if __debug__: log(str(response.text))
          errCheck = True
          dictResponse = dict()
          skipProc = False
          try:
            dictResponse = json.loads(str(response.text))
            errCheck = False
          except Exception as e :
            skipProc = True
            if __debug__: log('oh no: ' + str(response.text))
          if not skipProc:   
            if (response.status_code==200): # good server
              #pull_db = Discord_Server.objects.get(id=server.id)
              now = str(int(round(time.time())))    
              online = str(dictResponse['approximate_presence_count'])
              total = str(dictResponse['approximate_member_count'])
              log('Online: ' + online + ' Total: ' + total)
              if server.tags is None:
                server.tags = [ str( str(now) + '|' + online + '|' + total) ]

              else:
                server.tags.append( str( str(now) + '|' + online + '|' + total) )
              server.approximate_presence_count = dictResponse['approximate_presence_count']
              server.approximate_member_count = dictResponse['approximate_member_count']
              server.time_last_count = now
              server.save(update_fields = ['tags', 'approximate_presence_count', 'approximate_member_count', 'time_last_count'])

              updated += 1
              if __debug__: log('Sucessfully updated ' + dictResponse['guild']['name'] 
                            + ' [' + invite + '] with counts [' + str(dictResponse['approximate_presence_count']) 
                            + ',' + str(dictResponse['approximate_member_count']) + ']')


            elif (response.status_code == 429):
              if __debug__: log('RATE LIMIT HIT! Cycling proxy..')
              self.cycleProxy()
            elif (response.status_code == 404):
              log('Server has been deleted -- deleting ' + server.guild_name)
              server.delete()
            
            else:
              log('unhandled response: ' + str(response.status_code))
                #exit()
        else:
          self.cycleProxy()



  def get(self, request, *args, **kwargs):
    error_response = {'error': 'invalid request'}
    if kwargs is not None:
      action = kwargs['action']
      if action is None:
        return HttpResponse(error_response)     
 
      if action == 'count':
        start_at = int(request.GET.get('start_at', 0))
        amount = int(request.GET.get('amount', 0))
        log('fetching counts for : ' + str(amount) + ' items starting at ' + str(start_at))
        if int(start_at) > 0:
          self.updateCounts(start_at, amount)
          return HttpResponse({'foo':'bar'})

    return HttpResponse(str(error_response))  
