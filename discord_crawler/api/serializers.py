from rest_framework import serializers
from discord_crawler.models import Discord_Server

class Discord_ServerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Discord_Server
        fields = ('__all__')