from .serializers import Discord_ServerSerializer
from discord_crawler.models import *
from rest_framework import viewsets
from rest_framework.response import Response
from django.views.decorators.cache import never_cache
from rest_framework.pagination import PageNumberPagination

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'size'
    max_page_size = 300


class Discord_ServerViewSet(viewsets.ViewSet):
    err_response = {'error', 'bad response'}
    pagination_class = StandardResultsSetPagination

    @never_cache
    def list(self, request, *args, **kwargs):
        if request.method == 'GET':
            list_all = True
            parameters = request.GET
            page = 1
            queryset = None

            if parameters is not None:
                # YEET POTENTIAL FILTER PARAMETERS WITH DEFAULTS SET
                min_id      = int(request.GET.get('min_id', -1))
                name        = str(request.GET.get('name', 'undefined'))
                min_on      = int(request.GET.get('min_on', 0))
                min_total   = int(request.GET.get('min_total', 0))
                sort        = request.GET.get('sort', 'default')
                pagesize    = int(request.GET.get('pagesize', '100'))

                # YEET FILTERS ONE BY ONE
                if min_id > 15000: # FILTER A CHUNK OF IDS (LIVEFETCH)
                    list_all = False
                    queryset = Discord_Server.objects.filter(id__gte=last_received) # NEW YEET

                if min_on > 0: # FILTER A MINIMUM ONLINE COUNT
                    list_all = False
                    if queryset is not None: # CHECK EXISTING YEETS
                        queryset = queryset.filter(approximate_presence_count__gte=min_on)
                    else: # NEW YEET
                        queryset = Discord_Server.objects.filter(approximate_presence_count__gte=min_on)


                if min_total > 0: # FILTER A MINIMUM TOTAL COUNT
                    list_all = False
                    if queryset is not None: # CHECK EXISTING YEETS
                        queryset = queryset.filter(approximate_member_count__gte=min_on)
                    else: # NEW YEET
                        queryset = Discord_Server.objects.filter(approximate_member_count__gte=min_on)

                if name is not 'undefined': # FILTER BY GUILD NAMES
                    list_all = False
                    if queryset is not None: # CHECK EXISTING YEETS
                        queryset = queryset.filter(guild_name__icontains = name)
                    else: # NEW YEET
                        queryset = Discord_Server.objects.filter(guild_name__icontains = name)   


            if list_all: # UNFILTERED RESULTS
              queryset = Discord_Server.objects.all()
              
              # SORT METHOD REQUIRED
              if 'default' in sort:
                print('insert verbose default comment')
              elif 'online_asc' in sort:
                queryset = queryset.order_by('approximate_online_count')
              elif 'online_desc' in sort:
                queryset = queryset.order_by('-approximate_online_count')
              elif 'total_asc' in sort:
                queryset = queryset.order_by('approximate_member_count')
              elif 'total_desc' in sort:
                queryset = queryset.order_by('-approximate_member_count')
              elif 'date_asc' in sort:
                queryset = queryset.order_by('time_added')
              elif 'date_desc' in sort:
                queryset = queryset.order_by('-time_added')
              elif 'recrawl_asc' in sort:
                queryset = queryset.order_by('time_last_count')
              elif 'recrawl_desc' in sort:
                queryset = queryset.order_by('-time_last_count')  

              if queryset is not None:
                paginator = PageNumberPagination()
                paginator.page_size = pagesize
                result_page = paginator.paginate_queryset(queryset, request)
                serializer = Discord_ServerSerializer(result_page, many = True)
                return paginator.get_paginated_response(serializer.data)

            else:

              if 'default' in sort:
                print('insert verbose default comment')
              elif 'online_asc' in sort:
                queryset = queryset.order_by('approximate_online_count')
              elif 'online_desc' in sort:
                queryset = queryset.order_by('-approximate_online_count')
              elif 'total_asc' in sort:
                queryset = queryset.order_by('approximate_member_count')
              elif 'total_desc' in sort:
                queryset = queryset.order_by('-approximate_member_count')
              elif 'date_asc' in sort:
                queryset = queryset.order_by('time_added')
              elif 'date_desc' in sort:
                queryset = queryset.order_by('-time_added')
              elif 'recrawl_asc' in sort:
                queryset = queryset.order_by('time_last_count')
              elif 'recrawl_desc' in sort:
                queryset = queryset.order_by('-time_last_count') 
                
              paginator = PageNumberPagination()
              paginator.page_size = pagesize
              result_page = paginator.paginate_queryset(queryset, request)
    
              serializer = Discord_ServerSerializer(result_page, many=True)
              return paginator.get_paginated_response(serializer.data)     

        return Response(self.err_response)