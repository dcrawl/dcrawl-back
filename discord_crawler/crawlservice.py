# Crawls pushshift and adds unique links to discord_crawler_discord_server database
# v0.5.5 testing 8/2/19
# - converted to celery task
# -------------------------------------------------------------------------------------\

import psycopg2, psycopg2.extras
import urllib.parse
import sys, json, re, requests, sys, time, os
from free_proxy import FreeProxy
from django.core.cache import cache
from .models import Discord_Server, invalid_link

if __debug__:
  import traceback
  import logging

def log(msg=None, isError=False, e=None):
  if not isError and msg:
    print('*** [dcrawl-svc] ' + msg + ' \n')
  else:
    print ('*** [ERROR] ' + str(sys.exc_info()[1])  + ' \n')
    if __debug__: logging.error(traceback.format_exc())

# -------------------------------------------------------------------------------------\

class CrawlService():
  conn                = None
  dict_cur            = None
  cached_guild_ids    = list()
  cached_invite_links = list()
  cached_invalid      = list()
  proxy               = None
  ps_api              = 'https://api.pushshift.io/reddit/search/comment/?q=discord.gg&size=300&field=body'
  discord_api         = 'https://discordapp.com/api/v6/invites/' #invitecode?&with_counts=true
  headers             = { "Accept": "*/*", "Accept-Encoding": "gzip, deflate, br", "Accept-Language": "en-US",  \
                          "Referer": "http://reddit.com/", "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64)   \
                          AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
                          "X-HTTP-Method": "GET" }

  # ------------------------------------------------------------------------------------------------------------\
  def cycleProxy(self):
    keepChecking = True
    iters = 0
    while keepChecking is True:
      try:
        iters += 1
        if iters > 20:
          keepChecking = False
          log('Failed 20 times, quitting crawl..')
          cache.set('CRAWL_STATUS', 'Idle')
        proxy = FreeProxy(rand=True).get()
        proxyDict = {"https" : proxy }  
        if __debug__: log('Using proxy ' + str(proxyDict))
        self.proxy = proxyDict
        keepChecking = False
      except:
        log('Trying another proxy.. (cycle ' + str(iters) + ')')
        keepChecking = True


  # ------------------------------------------------------------------------------------------------------------\
  def generateEndPoint(self, from_code):
    return(self.discord_api + from_code + '?with_counts=true')

  # ------------------------------------------------------------------------------------------------------------\
  def getPushshiftComments(self):
    a_list = list()
    contents = urllib.request.urlopen(self.ps_api).read()
    json_data = json.loads(contents)['data']
    for comment in json_data:
      another_list = self.parseInvitesFromComment(comment['body'])
      the_subreddit = comment['subreddit']
      the_parentid = comment['parent_id']
      if another_list:
        for cmt in another_list:
          a_list.append( {  'subreddit': the_subreddit, 
                            'invite_link': cmt,
                            'parent_id': the_parentid  } )          
    return a_list


  # ------------------------------------------------------------------------------------------------------------\
  def checkIDExists(self, id):
    if Discord_Server.objects.filter(guild_id=str(id)).exists():
      #log('id ' + str(id) + ' exists...')
      return True
    return False

  # ------------------------------------------------------------------------------------------------------------\
  def checkLinkValid(self, link):
    if Discord_Server.objects.filter(invite_link=str(link)).exists():
      return True
    return False

  # ------------------------------------------------------------------------------------------------------------\
  def checkLinkInvalid(self, link):
    if invalid_link.objects.filter(invite_link=str(link)).exists():
      return True
    return False

  # ------------------------------------------------------------------------------------------------------------\
  def checkNSFW(self, parent_id):
    detail_api  = 'https://api.pushshift.io/reddit/search/submission/?ids=' + parent_id
    try:
      contents = urllib.request.urlopen(detail_api).read()
      json_data = json.loads(contents)['data']
      over_18 = str(json_data[0]['over_18'])
      if over_18 is 'True':
        return 1
      elif over_18 is 'False':
        return 0
      else:
        return 2
    except:
      return 2


    # ------------------------------------------------------------------------------------------------------------\
  def addCrawledServer(self, approximate_member_count, approximate_presence_count, 
                           invite_link, guild_icon, guild_id, guild_name, guild_splash, 
                           guild_description, subreddit, nsfw):
    try:
      now = str(int(round(time.time())))                
      this_server = Discord_Server.objects.create(approximate_member_count=approximate_member_count, 
                                                  approximate_presence_count=approximate_presence_count,
                                                  invite_link=invite_link,
                                                  guild_icon=guild_icon,
                                                  guild_id=guild_id,
                                                  guild_name=guild_name,
                                                  guild_splash=guild_splash,
                                                  guild_description=guild_description,
                                                  related_subredit=subreddit,
                                                  nsfw=nsfw,
                                                  time_added=now,
                                                  time_last_count=now)
      this_server.save()
      return True
    except Exception as e:
      log(None, True, e)
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def addInvalidServer( self, invite_link ):
    try:
      this_invalid_link = invalid_link.objects.create(invite_link = invite_link)
      this_invalid_link.save()
      return True
    except Exception as e:
      log(None, True, e)
      return False

  # ------------------------------------------------------------------------------------------------------------\
  def parseInvitesFromComment(self, comment):
    regExp = r'\(([^()]*)\)'
    matches = list()
    found = list()
    matches = re.findall(regExp, comment)  
    ct = 0
    for link in matches:
      try       :
        invite = link.split('.gg/')[1] 
        ct += 1
        found.append(invite) 
      except Exception as e: pass
    if len(found) > 0:  
      return found
    else: 
      words = None
      code = None
      is_words = comment.find(' ')
      if is_words > -1:
        words = comment.split(' ')
        for word in words:
          is_disclink = word.find('.gg/')
          if is_disclink > -1:
            code = word.split('.gg/')
            if code: code = code[1]
      else:
        is_disclink = comment.find('.gg/')
        if is_disclink > -1:
          code = comment.split('.gg/')[1]
      if code:
        found.append(code)
        return [code]
      else:
        return None

  # ------------------------------------------------------------------------------------------------------------\
  def crawl(self):
    ps_comments =  self.getPushshiftComments ()
    to_crawl          =  list()

    if ps_comments:
      for link in ps_comments:
        skipThisLink = False
        invite = link ['invite_link']
        subreddit = link ['subreddit']
        parent = link ['parent_id']
        if self.checkLinkValid(invite) is True:
          skipThisLink = True
        if self.checkLinkInvalid(invite) is True: skipThisLink = True
        if skipThisLink is False:
          if (len(invite)>3) & (len(invite)<8):
            strpos = invite.find(' ')
            if (strpos>0): next
            strpos = invite.find('#')
            if (strpos>0): next
            strpos = invite.find('"')
            if (strpos>0): next
            strpos = invite.find('-')
            if (strpos>0): next
            strpos = invite.find(':')
            if (strpos>0): next                       
            to_crawl.append( { 'subreddit'   : subreddit, 
                               'invite_link' : str(invite),
                               'parent_id'   : parent   } )

      log('Found ' + str(len(to_crawl)) + ' new links to crawl\n' )
      if len(to_crawl) > 0:
        self.resolveInviteLinks(to_crawl)

  # ------------------------------------------------------------------------------------------------------------\
  def resolveInviteLinks(self, links):
    self.cycleProxy  ()
    crawled           = 0
    for link in links : 
      invite = link['invite_link']
      subreddit = link['subreddit']
      parent = link['parent_id']
      noBueno = False
      if  self.checkLinkValid(invite) is not True and self.checkLinkInvalid(invite) is not True:
        endpoint = self.generateEndPoint(invite)
        if __debug__: log('requesting server data from ' + endpoint)
        try:
          response = requests.get(endpoint, headers = self.headers  ,
                                            proxies = self.proxy    )     
        except Exception as e:
          log('cycling: ' + str(e))
          noBueno  = True
        if not noBueno:
          if __debug__: log(str(response.text))
          errCheck = True
          dictResponse = dict()
          skipProc = False
          try:
            dictResponse  = json.loads(str(response.text))
            errCheck = False
          except Exception as e:
            skipProc = True
            if __debug__: log('caught exception:  ' + str(response.text))
          if not skipProc:   
            if (response.status_code==200): 
              if self.checkIDExists(dictResponse['guild']['id']):
                self.addInvalidServer(dictResponse['code'])
              else:
                nsfwSatus = self.checkNSFW(parent)
                self.addCrawledServer(  dictResponse['approximate_member_count'], dictResponse['approximate_presence_count'], dictResponse['code'], dictResponse['guild']['icon'], dictResponse['guild']['id'], dictResponse['guild']['name'], dictResponse['guild']['splash'], dictResponse['guild']['description'], subreddit, nsfwSatus)
                crawled += 1
                if __debug__: log('sucessfully crawled ' + dictResponse['guild']['name'] + ' [' + invite + '] with NSFW status ' + str(nsfwSatus))
            elif (response.status_code == 429):
              if __debug__: log('cycling: rate limited')
              self.cycleProxy()
            elif (response.status_code == 404):
              self.addInvalidServer(invite)
              if __debug__: log('404 invalid server response')
            else:
                log('unhandled response: ' + str(response.status_code))
                #exit()
        else:
          self.cycleProxy()
                    
# ------------------------------------------------------------------------------------------------------------\

  # ------------------------------------------------------------------------------------------------------------\
  def updateCounts(self, start_at, amount):
    self.cycleProxy()
    updated = 0
    list_all = False
    queryset = Discord_Server.objects.filter(id__gte=start_at)
    queryset = queryset.filter(id__lte=(start_at+amount))

    for server in queryset : 
     
      invite = server.invite_link
      oldcounts = server.counts
      log('old counts: ' + str(oldcounts) + '\n')
      noBueno = False
    
      endpoint   = self.generateEndPoint(invite)
      if __debug__: log('yeeting digits from ' + endpoint)
      try:
        response = requests.get(endpoint, headers = self.headers  ,
                                          proxies = self.proxy    )     
      except Exception as e:
        log('caught exception: ' + str(e))
        noBueno  = True
      if not noBueno:
        if __debug__: log(str(response.text))
        errCheck = True
        dictResponse = dict()
        skipProc = False
        try:
          dictResponse = json.loads(str(response.text))
          errCheck = False
        except Exception as e :
          skipProc = True
          if __debug__: log('oh no: ' + str(response.text))
        if not skipProc:   
          if (response.status_code==200): # good server
          
            updated += 1
            if __debug__: log('Sucessfully updated ' + dictResponse['guild']['name'] 
                          + ' [' + invite + '] with counts [' + dictResponse['approximate_presence_count'] 
                          + ',' + dictResponse['approximate_member_count'] + ']')


          elif (response.status_code == 429):
            if __debug__: log('RATE LIMIT HIT! Cycling proxy..')
            self.cycleProxy()
         
          else:
            log('unhandled response: ' + str(response.status_code))
              #exit()
      else:
        self.cycleProxy()

