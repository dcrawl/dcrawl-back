
from django.http import HttpResponse
from django.conf import settings
from django.views import View
from .models import Discord_Server 
from django.template.context_processors import csrf
import sys, json, re, requests, sys, time, os
from django.core.cache import cache


def log(str):
  print('*** [ddupe-kill] ' + str + '\n')

class DupeKiller(View):
  model   = Discord_Server
  action  = None


  # ------------------------------------------------------------------------------------------------------------\
  def killDupes(self, amount):
    
    killed = 0
    queryset = Discord_Server.objects.all() 
    queryset2 = Discord_Server.objects.all() 
    for server in queryset:
      this_guild_id = server.guild_id
      for server2 in queryset2:
        if killed < amount:
          this_guild_id2 = server2.guild_id
          #log(str(this_guild_id) + ' | ' + str(this_guild_id2))
          if this_guild_id == this_guild_id2:    
            if server.id != server2.id:        
              log('FOUND A FUCKING DUPE: ' + str(server.id) + ' MATCHES ' + str(server2.id))
              try:
                server2.delete()
                killed += 1
              except:
                pass
              
        else:
          log('Reached amount, exiting')
          exit()


  def get(self, request, *args, **kwargs):
    error_response = {'error': 'invalid request'}
    if kwargs is not None:
      action = kwargs['action']
      if action is None:
        return HttpResponse(error_response)     
 
      if action == 'kill':
       
        amount = int(request.GET.get('amount', 0))
        log('killing ' + str(amount) + ' dupes')
        if int(amount) > 0:
          self.killDupes(amount)
          return HttpResponse({'foo':'bar'})

    return HttpResponse(str(error_response))  
