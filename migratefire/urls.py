from django.urls import path
from migratefire.views import View, FireMigration
import json



urlpatterns = [
    # /migrate/fromjson/40/900 will try to migrate 900 servers starting at the 40th
     path(r'<slug:action>', FireMigration.as_view(), name='migrate')

] 
