# Firebase->PostregSQL migrator
# & non-cached early API 

import json
from django.http import HttpResponse
from django.conf import settings
from django.views import View
from discord_crawler.models import Server_DefaultChannel, Server_Inviter, Server_Guild, Discord_Server
from django.template.context_processors import csrf


# laziness
# -----------------------------------------------------------------------------\

def log(str):
  print('*** [migrate] ' + str + '\n')

# class instance for migration app/view
# -----------------------------------------------------------------------------\
class FireMigration(View):
  model   = Discord_Server
  action  = None

  def post(self, request):
    log('POST called')  

  # flask-style plain request handling
  # -----------------------------------------------------------------------------\
  def get(self, request, *args, **kwargs):
    error_response = {'error': 'invalid request'}
    if kwargs is not None:
      action = kwargs['action']
      if action is None:
        return HttpResponse(str(error_response))       
 
      if action == 'migrate':
        start_at = int(request.GET.get('start_at', -1))
        amount = int(request.GET.get('amount', -1))
        log('start_at and amount retrieved: ' + str(start_at) + ' / ' + str(amount))
        if int(start_at) > -1 and int(amount) > 1:
          log("calling pull")
          migrated_count = self.pull(amount, start_at)
          log("done, calculating response")
          json_response = {'result': 'success', 'migrated' : str(migrated_count)}
          log("returning json")
          return HttpResponse(str(json_response))

    return HttpResponse(str(error_response))  



# This will not update data, it will only add new data (intended functionality.)
# If it fails on a key, increase the start_at value or set check_for_existing to True
# unique position in the migrated firebase table and is not always incramented by 1 
# (but 90% of the time, it is.)  For adding new servers just start at the latest 
#  server_key and go up. 
# -----------------------------------------------------------------------------\
  def pull(self, amt, start_at, check_for_existing = True):
    log("Pull called: " + str(amt) + " " + str(start_at))
    # so that multiple clients can iterate over this object simultaneously
    copy_data = settings.SERVER_DATA # use .clear() to prevent memory leak

    if copy_data is not None:
      pulled_ct=0
      saved_ct=0
      
      for invite,data in copy_data.items():
        if pulled_ct >=  amt:
          return saved_ct
        pulled_ct += 1
        if (pulled_ct > start_at):
          log(str(data))
          skipThis = False  
          if check_for_existing is True:        
            try:
              does_it_exist = Discord_Server.objects.get(server_key=pulled_ct)
              if does_it_exist is not None:
                log('Server ' +  str(pulled_ct) + ' was skipped because its already in the database.')
                skipThis = True
                next
            except:
              skipThis = False

          # -----------------------------------------------------------------------------\
          # EXAMINE DATA INTEGRITY BEFORE EVEN TRYING
          try:
            dataGuild = data['guild']
            dataChannel = data['channel']
          except:
            log('Skipping server ' + str(pulled_ct) + ' [failed guild/channel integrity check]')
            skipThis = True

          try:
            aCode = data['code']

          except:
            log('Skipping server ' + str(pulled_ct) + ' [failed invite integrity check]')
            skipThis = True
  
          if skipThis is not True:

            # -----------------------------------------------------------------------------\ 
            # SERVER CONTAINER MODEL
            this_server = Discord_Server.objects.create(server_key=pulled_ct)
            this_server.id = pulled_ct + 20
            this_server.invite_link = data['code']

            # -----------------------------------------------------------------------------\
            # GUILD MODEL (inherited from Server)

            this_guild = Server_Guild.objects.create()
            this_guild.name = dataGuild['name']
            this_guild.guild_id = dataGuild['id']

            # -----------------------------------------------------------------------------\
            # FAILSAFE GUILD DATA
            try:
              this_guild.icon = dataGuild['icon']
            except:
              this_guild.icon = None
            try:
              this_guild.features = dataGuild['features']
            except:
              this_guild.features = None
            try:
              this_guild.description = dataGuild['description']
            except:
              this_guild.description = None
            try:
              this_guild.splash = dataGuild['splash']
            except:
              this_guild.splash = None

            # -----------------------------------------------------------------------------\
            # INVITER DATA (inherited from Server)  
            try:
              dataInviter = data['inviter']
              this_inviter = Server_Inviter.objects.create()
              this_inviter.username=dataInviter['username']
              this_inviter.avatar = dataInviter['avatar']
              this_inviter.inviter_id = dataInviter['id']
              this_inviter.discriminator = dataInviter['discriminator']
            except:
              this_inviter = None

            # -----------------------------------------------------------------------------\
            # FAILSAFE CHANNEL DATA (inherited from Server)
            this_def_chan = Server_DefaultChannel.objects.create()
            try:
              this_def_chan.name = dataChannel['name']
            except:
              this_def_chan.name = None
            try:
              this_def_chan.channel_id = dataChannel['id']
            except:
              this_def_chan.channel_id = 0
            try:
              this_def_chan.channel_type = dataChannel['type']
            except:
              this_def_chan.channel_type = 0

            # -----------------------------------------------------------------------------\
            # FAILSAFE VARIANCE DATA (my fault)
            try:
              this_server.approximate_member_count = data['approximate_member_count']
              this_server.approximate_presence_count = data['approximate_presence_count']  
            except:
              try:
                this_server.approximate_member_count = data['users']['total']
                this_server.approximate_presence_count = data['users']['online']
              except: # if this happens something went terribly wrong. EJECT
                log('EXITING: Fatal error obtaining server counts of server ' + str(data))  
                copy_data.clear()
                exit()

            # -----------------------------------------------------------------------------\
            # PUT THE MODELS TOGETHER
            this_server.server_guild = this_guild
            this_server.server_defaultchannel = this_def_chan
            this_server.server_inviter = this_inviter
            this_server.save()

            # -----------------------------------------------------------------------------\
            # SAVE AS NEEDED (SQL)
            if (this_guild): 
              this_guild.save()
            if this_def_chan:
              this_def_chan.save()
            if this_inviter:
              this_inviter.save()
            
            log('Saved server ' + this_server.invite_link + ' [' + str(pulled_ct) + 'of ' + str(amt) + ']')
            saved_ct += 1

        else: # skip this index (implements start_at parameter)
          next

      copy_data.clear() # prevent memory leak
      return saved_ct 

